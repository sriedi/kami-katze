﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject[] cameras;
    public bool multiplayer = false;

    private void Awake()
    {
        Scoring.playing = false;
        Scoring.multiplayer = multiplayer;
    }

    private void OnTriggerEnter(Collider other)
    {
        other.enabled = false;

        if (other.CompareTag("PlayerBody") && !Scoring.playing)
        {
          //  print("called");
            if (!multiplayer)
            {
                cameras[0].SetActive(false);
                cameras[1].SetActive(true);
            }
            Scoring.playing = true;
        }

        //gameObject.SetActive(false);
    }
}
