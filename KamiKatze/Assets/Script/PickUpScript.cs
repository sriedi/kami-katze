﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpScript : MonoBehaviour
{
    public Shot randomPickup;

    private void Awake()
    {
        GameObjectManager.pickUps.Add(this);
        randomPickup = GetRandomEnum<Shot>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            PlayerShooting o = other.GetComponent<PlayerShooting>();

            if (randomPickup != Shot.shield)
            {
                o.pickup = randomPickup;
                o.firing = false;
            } else
            {
                o.ActivateShield();
            }
            GameObjectManager.pickUps.Remove(this);
            GameObjectManager.RemovePickupFromAI(this);
            Destroy(gameObject);
        }

        if(other.CompareTag("NPC"))
        {
            print(randomPickup);
            EnemyShooting o = other.GetComponent<EnemyShooting>();

            if (randomPickup != Shot.shield)
            {
                o.pickup = randomPickup;
            }
            else
            {
                o.ActivateShield();
            }

            o.brain.nextPickup = null;
            GameObjectManager.pickUps.Remove(this);
            GameObjectManager.RemovePickupFromAI(this);
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        GameObjectManager.pickUps.Remove(this);
        GameObjectManager.RemovePickupFromAI(this);
    }

    static T GetRandomEnum<T>()
    {
        System.Array A = System.Enum.GetValues(typeof(T));
        T V = (T)A.GetValue(UnityEngine.Random.Range(1, A.Length));
        return V;
    }
}
