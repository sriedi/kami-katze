﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboarding : MonoBehaviour
{
    Transform playerCamera;

    // Start is called before the first frame update
    void Start()
    {
        playerCamera = transform.parent.GetComponent<PlayerMovement>().mainCam.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = playerCamera.rotation;
    }
}
