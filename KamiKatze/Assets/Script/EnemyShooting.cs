﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Transform[] muzzles;
    public Transform[] shotgunMuzzles;
    public List<GameObject> guns = new List<GameObject>();
    public List<GameObject> shotguns = new List<GameObject>();
    public GameObject rocketProb;
    public GameObject mineProb;
    public Transform specialMuzzle;
    public GameObject bullet;
    public GameObject targetSeekingRocket;
    public GameObject mine;
    public Transform shield;
    public Transform sensor;
    [HideInInspector]
    public EnemyBehaviour brain;

    public Shot pickup;

    [Header("Player Detection")]
    public float fov = 60;
    float initialFov;

    [Header("Shooting Behaviour")]
    public float shootDistance = 12;
    float initialShootDistance;
    public float intervalTime;
    float initialIntervalTime;
    float timer;
    int maxPickupIterations = 30;
    int shotGunIterations = 3;
    bool firing = false;
    //GunEmitter from the GunsoundEmitter Prefab
    GunEmitter gunSoundEmitter;

    [Header("UI Handling")]
    public UiHandler ui;

    // Start is called before the first frame update
    void Start()
    {
        brain = GetComponent<EnemyBehaviour>();
        gunSoundEmitter = gameObject.GetComponentInChildren<GunEmitter>();
        timer = intervalTime;
        shield.gameObject.SetActive(false);
        
        // Set initial Values
        initialIntervalTime = intervalTime;
        initialFov = fov;
        initialShootDistance = shootDistance;

        foreach (Transform m in muzzles)
        {
            if (m.Find("RapidFire")) //name.Contains("RapdiFire"))
            {
                GameObject g = m.Find("RapidFire").gameObject;
                guns.Add(g);
                g.SetActive(false);
            }
        }

        foreach (Transform n in shotgunMuzzles)
        {
            if (n.Find("Shotgun")) //name.Contains("RapdiFire"))
            {
                GameObject g = n.Find("Shotgun").gameObject;
                shotguns.Add(g);
                g.SetActive(false);
            }

            if (n.Find("Mine"))
            {
                mineProb = n.Find("Mine").gameObject;
                mineProb.SetActive(false);
            }

            if (n.Find("Rocket"))
            {
                rocketProb = n.Find("Rocket").gameObject;
                rocketProb.SetActive(false);
            }
        }
    }

    private void Update()
    {
        DisplayGuns();

        ui.pickUp = pickup;
        // Check for Target and afterwards if Target is in Sight. If true, allow Shooting
        if (brain.target != null)
        {
            if (PlayerInSight())
            {
                if (pickup != Shot.burst)
                {
                    firing = true;
                }
                else
                {
                    firing = true;
                    timer = 0;
                }
            } 
        }

        // Start Shooting
        if(firing)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                Shooting();
                timer = intervalTime;
            }
        }

        // Trigger Pickup search Behaviour depending on if NPC already has a PickUp
        if(pickup == Shot.normal)
        {
            brain.getPickUp = true;
        } else
        {
            brain.getPickUp = false;
        }

        // Trigger Statemachine depeding on current PickUp
        if(brain.nextPickup == null)
        {
            if(pickup != Shot.mine)
            {
                // Get BehindPlayer
                brain.state = State.following;
            } else
            {
                // Get in Front of player
                brain.state = State.pursue;
            }
        }
    }

    public void DisplayGuns()
    {
        if (pickup == Shot.burst)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(true);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        } else if(pickup == Shot.searching)
        {
            rocketProb.SetActive(true);
            mineProb.SetActive(false);

            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }
        }
        else if(pickup == Shot.mine)
        {
            mineProb.SetActive(true);
            rocketProb.SetActive(false);

            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }
        }
        else if (pickup == Shot.shotgun)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(true);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        }
        else if(pickup == Shot.normal)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        }
    }

    public void Shooting()
    {
        if (pickup == Shot.normal)
        {
            
            foreach (Transform t in muzzles)
            {
                gunSoundEmitter.ActionSound("gun");
                GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                g.GetComponent<BulletBehaviour>().parent = transform;
            }
        }
        else if (pickup == Shot.searching)
        {
            fov *= 3;
            GameObject g = Instantiate(targetSeekingRocket, specialMuzzle.transform.position, specialMuzzle.transform.rotation, null);
            g.GetComponent<SeekingRocket>().parent = transform;
            g.GetComponent<SeekingRocket>().shield = shield;
            ResetFov();
            pickup = Shot.normal;
        }
        else if (pickup == Shot.mine)
        {
            //fov *= 0.5f;
            fov *= 3;
            brain.state = State.pursue;
            brain.speed *= 2.5f;

            if (shotGunIterations-2 >= 0)
            {
                GameObject g = Instantiate(mine, specialMuzzle.transform.position, specialMuzzle.transform.rotation, null);
                g.GetComponent<MineBehaviour>().parent = transform;
                g.GetComponent<MineBehaviour>().shield = shield;

                shotGunIterations--;
            }
            else
            {
                pickup = Shot.normal;
                shotGunIterations = 3;
                // if Mine is dropped, change to normal pursue Behaviour
                brain.state = State.following;
                ResetFov();
            }
        }
        else if (pickup == Shot.burst)
        {
            fov *= 0.5f;

            if (maxPickupIterations >= 0)
            {
                maxPickupIterations--;
                intervalTime = initialIntervalTime * 0.2f;
                gunSoundEmitter.ActionSound("gun");
                foreach (Transform t in muzzles)
                {
                    GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                    g.GetComponent<BulletBehaviour>().parent = transform;
                }
            }
            else
            {
                intervalTime = initialIntervalTime;
                maxPickupIterations = 30;
              //   firing = false;
                ResetFov();
                pickup = Shot.normal;
            }
        } else if (pickup == Shot.shotgun)
        {
            print("doing");
            fov *= 1.5f;
            shootDistance *= 0.8f;

            if (shotGunIterations >= 0)
            {
                shotGunIterations--;
                intervalTime = initialIntervalTime * 3f;
                gunSoundEmitter.ActionSound("shotgun");
                foreach (Transform t in shotgunMuzzles)
                {
                    GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                    BulletBehaviour b = g.GetComponent<BulletBehaviour>();
                    b.parent = transform;
                    b.lifetime *= 0.2f;
                }
            }
            else
            {
                shootDistance = initialShootDistance;
                shotGunIterations = 3;
              //  firing = false;
                ResetFov();
                pickup = Shot.normal;
            }
        }
    }

    public bool PlayerInSight()
    {
        RaycastHit hit;
        float lookAngle;

        // Define look direction of NPC depending on Steering Behaviour
          if (brain.state == State.pursue)
          {
            lookAngle = Vector3.Angle(-transform.right, brain.target.position - transform.position);
          } else 
          {
            lookAngle = Vector3.Angle(transform.right, brain.target.position - transform.position);
          } 

        // Check if Target is in LookDirection
        if (lookAngle < fov * 0.5f) 
        {
            // Check if Player is visible
            if (Physics.Raycast(transform.position, brain.target.position - transform.position, out hit, shootDistance))
            {
                Debug.DrawRay(transform.position, brain.target.position - transform.position, Color.red, shootDistance);

                if (hit.transform == brain.target)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void ResetFov()
    {
        fov = initialFov;
        brain.speed = brain.maxSpeed;
    }

    public void ActivateShield()
    {
        brain.health.invincible = true;
        shield.gameObject.SetActive(true);
        StartCoroutine(DeactivateShield());
    }
    
    IEnumerator DeactivateShield()
    {
        yield return new WaitForSeconds(3);
        brain.health.invincible = false;
        shield.gameObject.SetActive(false);
    }
}
