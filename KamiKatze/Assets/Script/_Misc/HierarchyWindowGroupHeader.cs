﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[InitializeOnLoad]
public static class HierarchyWindowGroupHeader
{
	static HierarchyWindowGroupHeader()
	{
		EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
	}

	private const string EDITOR_ONLY_TAG = "EditorOnly";
	private const string PATTERN = @"^-{3,}(\\[mgbpdyorw])?(.+)-{3,}$";
	private static readonly Regex Regex = new Regex(PATTERN);
	private static readonly Dictionary<string, Color> Lookup = new Dictionary<string, Color>()
	{
		{ "\\m", new Color(22 / 255f, 160 / 255f, 133 / 255f) },
		{ "\\g", new Color(46 / 255f, 204 / 255f, 113 / 255f) },
		{ "\\b", new Color(41 / 255f, 128 / 255f, 185 / 255f) },
		{ "\\p", new Color(142 / 255f, 68 / 255f, 173 / 255f) },
		{ "\\d", new Color(44 / 255f, 62 / 255f, 80 / 255f) },
		{ "\\y", new Color(241 / 255f, 196 / 255f, 15 / 255f) },
		{ "\\o", new Color(230 / 255f, 126 / 255f, 34 / 255f) },
		{ "\\r", new Color(192 / 255f, 57 / 255f, 43 / 255f) },
		{ "\\w", new Color(236 / 255f, 240 / 255f, 241 / 255f) },
		{ "", new Color(41 / 255f, 128 / 255f, 185 / 255f) },
	};

	private static void HierarchyWindowItemOnGUI(int instanceId, Rect selectionRect)
	{
		if (Selection.Contains(instanceId))
			return;

		var gameObject = EditorUtility.InstanceIDToObject(instanceId) as GameObject;
		if (gameObject == null || !Regex.IsMatch(gameObject.name))
			return;

		Match match = Regex.Match(gameObject.name);
		Color color = StringToColor(match.Groups[1].Value);
		string name = match.Groups[2].Value;

		EditorGUI.DrawRect(selectionRect, color);
		selectionRect.y -= 2.5f;
		EditorGUI.DropShadowLabel(selectionRect, name);

		if (!gameObject.CompareTag(EDITOR_ONLY_TAG))
		{
			gameObject.tag = EDITOR_ONLY_TAG;
		}
	}

	private static Color StringToColor(string str)
	{
		return Lookup.ContainsKey(str) ? Lookup[str] : Lookup[""];
	}
}
#endif