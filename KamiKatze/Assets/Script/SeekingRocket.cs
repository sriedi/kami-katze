﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SeekingRocket : MonoBehaviour
{
    Vector3 desired;
    public Vector3 velocity;
    Vector3 seek;

    public Transform target;
    public float maxSpeed = 5f;
    public float speed = 25;
    public float maxSteerForce = 3f;

    bool seeking = false;
    public Transform parent;
    public Transform shield;
    Rigidbody rb;

    public GameObject impact;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        velocity = transform.forward * maxSpeed * Time.deltaTime;
    }

    private void Start()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 100f, LayerMask.GetMask("Targets")))
        {
            if (hit.collider.CompareTag("NPC") || hit.collider.CompareTag("Player"))
            {
                target = hit.collider.transform;
                StartCoroutine(StartSeek());
            }
            //target.position = hit.point;
        }
        else
        {
            if (GameObjectManager.targets.Count > 0 || GameObjectManager.players.Count > 0)
            {
                if (Scoring.multiplayer)
                {
                    target = GameObjectManager.GetOtherPlayer(parent);
                    print(target.name);
                    StartCoroutine(StartSeek());
                }
                else
                {
                    if (parent.CompareTag("Player"))
                    {
                        target = GameObjectManager.GetClosestTarget(transform.position);
                    }
                    else
                    {
                        target = GameObjectManager.GetClosestPlayer(transform.position);
                    }
                    StartCoroutine(StartSeek());
                }
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Reset desired
        Vector3 steering = Vector3.zero;
        seek = Vector3.zero;

        // reset desired Velocity, very Important for arrival Behaviour
        desired = Vector3.zero;

        if (seeking && target != null)
        {
            // Get direction
            seek = target.position - transform.position;

            // Create Steering Vector
            var seekForce = seek.normalized * maxSpeed;

            // Add Steering Vector to desired;
            desired += seekForce * 1;   
        }
        else
        {
            // Create Steering Vector
            var forwardForce = transform.forward.normalized * maxSpeed;

            // Add Steering Vector to desired;
            desired += forwardForce * 1;
        }

        // Create steering Force
        steering = desired - velocity;
        steering = Vector3.ClampMagnitude(steering, maxSteerForce);

        // Locomotion
        velocity = Vector3.ClampMagnitude(velocity + steering, maxSpeed);

        transform.position += velocity * Time.deltaTime;

        // Rotation
        transform.rotation = Quaternion.LookRotation(velocity);
    }

    Vector3 SteerTowards(Vector3 vector)
    {
        Vector3 v = vector.normalized * maxSpeed - velocity;
        return Vector3.ClampMagnitude(v, maxSteerForce);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform != parent)
        {
            if (collision.transform != shield && collision.transform.name !="Sensor")
            {
                if (collision.transform.CompareTag("NPC") || collision.transform.CompareTag("Player"))
                {
                    HealtManager h = collision.transform.GetComponent<HealtManager>();

                    if (!h.invincible)
                    {
                        h.Damage();
                    }
                }
                Instantiate(impact, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }

    IEnumerator StartSeek()
    {
        yield return new WaitForSeconds(0.7f);
        seeking = true;
    }
}
