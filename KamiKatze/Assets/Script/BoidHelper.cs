﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoidHelper {

    const int numViewDirections = 300;
    public static readonly Vector3[] directions;

    static BoidHelper () {
        directions = new Vector3[BoidHelper.numViewDirections];

        float goldenRatio = (1 + Mathf.Sqrt (5)) / 2;
        float angleIncrement = Mathf.PI * 4 * goldenRatio;

        for (int i = 0; i < numViewDirections; i++) {
            float t = (float) i / numViewDirections;
            float inclination = Mathf.Acos (1 - 2 * t);
            float azimuth = angleIncrement * i;

            float y = Mathf.Sin (inclination) * Mathf.Cos (azimuth);
         //   float z = Mathf.Sin (inclination) * Mathf.Sin (azimuth);
            float x = Mathf.Cos (inclination);
            float z = 0;
            directions[i] = new Vector3 (x, y, z);
        }
    }

}