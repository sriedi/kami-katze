﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class MineBehaviour : MonoBehaviour
{
    public Transform parent;
    public Transform shield;

    public GameObject impact;
    public StudioEventEmitter soundEmitterPlace;

    // Start is called before the first frame update
    void Start()
    {
        
        if(parent != null)
        {
            StartCoroutine(RemoveParent());
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform != parent)
        {
            if (collision.transform != shield)
            {
                if (collision.transform.CompareTag("NPC") || collision.transform.CompareTag("Player"))
                {
                    print("mine coll 1"+collision.name);
                    HealtManager h = collision.transform.GetComponent<HealtManager>();

                    if (!h.invincible)
                    {
                        h.Damage();
                    }

                    Instantiate(impact, transform.position, transform.rotation);
                    Destroy(gameObject);
                }
                else if (collision.name == "Shield")
                {
                    print("mine coll 1");

                    Instantiate(impact, transform.position, transform.rotation);
                    Destroy(gameObject);
                }

                /*   if(collision.name == "Shield" && collision.transform != shield)
               {
                   Instantiate(impact, transform.position, transform.rotation);
                   Destroy(gameObject);
               } */
            }
        }
     }
        

    IEnumerator RemoveParent()
    {
        soundEmitterPlace.Play();
        yield return new WaitForSeconds(1.5f);
        parent = null;
        shield = null;
    }
}
