﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Rewired;

public class GameRoundManager : MonoBehaviour
{
    public FadeController fade;
    public Text winnerText;
    public Text instructions;
    Canvas ui;
    public GameObject[] enemies;
    int round = 0;
    bool nextRound = false;

    Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = ReInput.players.GetPlayer(0);
        fade = FindObjectOfType<FadeController>();
        ui = GetComponent<Canvas>();
        instructions = transform.Find("Try Again Text").GetComponent<Text>();
        Scoring.roundManager = this;
        DeactivateUI();
        //  Scoring.playing = true;
        round = GameObjectManager.targets.Count;
    }

    private void Update()
    {
        if(!Scoring.playing && player.GetButtonDown("Shoot") && nextRound)
        {
            if (Scoring.multiplayer)
            {
                GameObjectManager.ClearManager();
                fade.FadeOut();
                StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex));
            }
            else
            {
                if (winnerText.text == "Enemies")
                {
                    GameObjectManager.ClearManager();
                    fade.FadeOut();
                    StartCoroutine(LoadLevel(1));
                }
                else if (winnerText.text == "Player")
                {
                    if (round != 3)
                    {
                        GameObjectManager.ClearManager();

                        fade.FadeOut();
                        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
                    }
                    else
                    {
                        GameObjectManager.ClearManager();
                        fade.FadeOut();
                        StartCoroutine(LoadLevel(0));
                    }
                }
            }
        }

        if (!Scoring.playing && player.GetButtonDown("X") && nextRound)
        {
            GameObjectManager.ClearManager();

            fade.FadeOut();
            StartCoroutine(LoadLevel(0));
        }
    }

    public void SetWinnerText(string t)
    {
        if (Scoring.playing && winnerText != null)
        {
            winnerText.text = t;
            ActivateUI();

            if (Scoring.multiplayer)
            {
                instructions.text = "Press [A] for a rematch. [X] to quit!";
            }
            else
            {
                if (t == "Player")
                {
                    if (round != 3)
                    {
                        instructions.text = "Press [A] for a bigger challenge. [X] to quit!";
                    }
                    else
                    {
                        instructions.text = "Congratulations! Press [A] to return to startscreen. [X] to quit!";
                    }
                }
                else if (t == "Enemies")
                {
                    instructions.text = "Press [A] to try agian!";
                }
            }

            StartCoroutine(EnableTransition());
            Scoring.playing = false;
        }
    }

    public void BeginNextRound()
    {
        if(round < 3)
        {
            round++;

            
            GameObjectManager.targets[round].gameObject.SetActive(true);
            GameObjectManager.targets[round].gameObject.GetComponent<EnemyShooting>().ui.gameObject.SetActive(true);
        } else
        {
            round = 2;
            // Endresult
        }
    }

    public void ActivateUI()
    {
        ui.enabled = true;
    }

    public void DeactivateUI()
    {
        ui.enabled = false;
    }

    IEnumerator EnableTransition()
    {
        yield return new WaitForSeconds(1);
        nextRound = true;
    }

    IEnumerator LoadLevel(int i)
    {
        yield return new WaitForSeconds(1.1f);
        SceneManager.LoadScene(i);
    }
}
