﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;


public class GunEmitter : MonoBehaviour
{
    public StudioEventEmitter regularGunSound;
    public StudioEventEmitter shotGunSound;
    // Start is called before the first frame update
 
 //Call with String for gun or shotgun
    public void ActionSound(string inString){

        switch (inString)
        {
            case "gun":
            regularGunSound.Play();
            break;

            case "shotgun":
            shotGunSound.Play();
            break;

            default: 
            regularGunSound.Play();
            break;
        }
    }


//In Case we need to stop the gunsound
  public  void StopSound(){
        regularGunSound.Stop();
        shotGunSound.Stop();
    }
}
