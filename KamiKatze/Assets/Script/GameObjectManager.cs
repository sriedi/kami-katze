﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectManager
{
    public static List<Transform> targets = new List<Transform>();
    public static List<Transform> players = new List<Transform>();
    public static List<EnemyBehaviour> targetBehaviours = new List<EnemyBehaviour>();
    public static List<PickUpScript> pickUps = new List<PickUpScript>();

    public static Transform GetClosestPickUp(Vector3 start)
    {
        if (Scoring.playing)
        {
            float d = 9999;

            if (pickUps.Count > 0)
            {
                Transform nearest = null;

                foreach (PickUpScript t in pickUps)
                {
                    if (Vector3.Distance(start, t.transform.position) < d)
                    {
                        d = Vector3.Distance(start, t.transform.position);
                        nearest = t.transform;
                    }
                }

                return nearest;
            }
            else
            {
                return null;
            }
        }

        return null;
    }

    public static Transform GetClosestTarget(Vector3 start)
    {
        float d = 9999;

        if(targets.Count > 0) {
            Transform nearest = null;

            foreach (Transform t in targets)
            {
                if (Vector3.Distance(start, t.position) < d)
                {
                    d = Vector3.Distance(start, t.position);
                    nearest = t;
                }
            }

            return nearest;
        } else
        {
            return null;
        }
    }

    public static Transform GetClosestPlayer(Vector3 start)
    {
        if (Scoring.playing)
        {
            float d = 9999;

            if (players.Count > 0)
            {
                Transform nearest = null;

                foreach (Transform t in players)
                {
                    if (Vector3.Distance(start, t.position) < d && t != null)
                    {
                        d = Vector3.Distance(start, t.position);
                        nearest = t;
                    }
                }

                return nearest;
            }
            else
            {
                return null;
            }
        } else
        {
            return null;
        }
    }

    public static Transform GetOtherPlayer(Transform own)
    {
        if (Scoring.playing)
        {
            if (players.Count > 0)
            {
                foreach (Transform t in players)
                {
                    if(t != own)
                    {
                        return t;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        return null;
    }

    public static void RemovePickupFromAI(PickUpScript p)
    {
        foreach(EnemyBehaviour eb in targetBehaviours)
        {
            if(p == eb.nextPickup)
            {
                eb.nextPickup = null;
            }
        }
    }

    public static string ReturnWinnerName(Transform t)
    {
        foreach (Transform p in players)
        {
            if (p != t)
            {
                return "" + p.gameObject.name;
            }
        }

        return "";
    }

    public static void ClearManager()
    {
        pickUps.Clear();
        players.Clear();
        targets.Clear();
        targetBehaviours.Clear();
    }
}
