﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    public Camera playerCam;
    Vector2 screenBounds;

    float camVertExtent;
    float camHorzExtent;

    // Start is called before the first frame update
    void Start()
    {
        // objectWidth = transform.lossyScale.x / 2;
        // objectHeight = transform.lossyScale.y / 2;

        camVertExtent = playerCam.orthographicSize;
        camHorzExtent = playerCam.aspect * camVertExtent;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        screenBounds = playerCam.ScreenToWorldPoint(new Vector3(camHorzExtent, camVertExtent, transform.position.z));
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x, screenBounds.x * -1);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y, screenBounds.y * -1);
        transform.position = viewPos;
    }
}
