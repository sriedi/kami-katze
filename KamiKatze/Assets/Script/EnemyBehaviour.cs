﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public enum State
{
    following,
    pursue,
    pickUp
}

public class EnemyBehaviour : MonoBehaviour
{
    [Header("FINITE STATEMACHINE")]
    public State state = State.following;

    [Header("STEERING")]
    public Transform target;
    Vector3 desired;
    [HideInInspector]
    public Vector3 velocity;
    Vector3 pursue;
    Vector3 evade;
    Vector3 avoidance;
    Vector3 separation;
    Vector3 alignment;

    Vector3 steering;
    PlayerBodyMovement targetMov;
    public float maxSpeed = 5f;
    public float maxSteerForce = 3f;
    public float speed;
    public float groupDistance = 6;
    public float arrivalDistance = 10;

    public float collisionAvoidDst = 1;
    public float boundsRadius = 1;

    public List<EnemyBehaviour> neighbours = new List<EnemyBehaviour>();

    [Header("WEIGHTS")]
    public float alignmentWeight = 1;
    public float separationWeight = 1;
    public float pursueWeight = 1;

    float initialPursueWeight;

    public float dist;
    Vector3 forwardDir;

    [Header("PICKUP LOGIC")]
    public Transform nextPickup;
    public float pickUpDistance = 10;
    public bool getPickUp = false;

    Vector3 pointToDraw;

    [Header("STATS")]
    public HealtManager health;
    Vector3 lastVelocity;

    //SoundEmitter
    StudioEventEmitter motorEmitter;
    private void Awake()
    {
        GameObjectManager.targets.Add(transform);
        GameObjectManager.targetBehaviours.Add(this);
        health = GetComponent<HealtManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        initialPursueWeight = pursueWeight;

        speed = maxSpeed;
        velocity = transform.forward * speed * Time.deltaTime;
        motorEmitter = gameObject.GetComponent<StudioEventEmitter>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        forwardDir = transform.right;

        if (Scoring.playing)
        {
            if (GameObjectManager.pickUps.Count > 0)
            {
                SetTarget();
            }

            // reset desired Velocity, very Important for arrival Behaviour in seek / follow / pursue
            desired = Vector3.zero;
            steering = Vector3.zero;
            velocity += transform.forward * speed * Time.deltaTime;

            // Group Behaviour
            FlockBehaviour();

            if (target != null)
            {
                // Statemachine
                switch (state)
                {
                    case State.following:
                        speed = maxSpeed;
                        FollowingBehaviour();
                        break;
                    case State.pickUp:
                        if (nextPickup != null)
                        {
                            speed = maxSpeed;
                            SeekBehaviour();
                        }
                        break;
                    case State.pursue:
                        speed = maxSpeed * 2;
                        PursuitBehaviour();
                        break;
                }
            }

            if (IsHeadingForCollision())
            {
                pursueWeight = 0;

                Vector3 collisionAvoidDir = ObstacleRays();
                Vector3 collisionAvoidForce = collisionAvoidDir * 40; // += separationForce * -separationWeight
                desired += collisionAvoidForce;
            }
            else
            {
                pursueWeight = initialPursueWeight;
            }

            // Create steering Force
            steering = desired - velocity;
            steering = Vector3.ClampMagnitude(steering, maxSteerForce);

            // Locomotion
            velocity = Vector3.ClampMagnitude(velocity + steering, speed);
            velocity.z = 0;
            transform.position += velocity * Time.deltaTime;

            transform.right = velocity;

            //float angle = AngleInDeg(velocity, desired);
            float angle = Vector3.Angle(lastVelocity, velocity);
             //Check Angle to Emit Sound
            if(angle >= 6f){
                StartCoroutine(SteeringSound());
            }
            // print(angle);
            float sign = Mathf.Sign(Vector3.Dot(lastVelocity, velocity));

            Vector3 pos = transform.position;
            pos.z = 0;
            transform.position = pos;

            transform.Rotate(new Vector3(angle * 10, 0, 0));
            lastVelocity = velocity;
        }
    }

    public void FlockBehaviour()
    {
        separation = Vector3.zero;
        alignment = Vector3.zero;

        if (neighbours.Count > 0)
        {
            dist = separationWeight;

            foreach (EnemyBehaviour eb in neighbours)
            {
                dist += Vector3.Distance(transform.position, eb.transform.position);

                separation += eb.transform.position - transform.position;
                alignment += eb.velocity;
            }

            separation /= neighbours.Count;
            alignment /= neighbours.Count;

            var separationForce = separation.normalized * speed;
            var alignmentForce = alignment.normalized * speed;

            desired += separationForce * -separationWeight;
            desired += alignmentForce * alignmentWeight;
        }
    }

    public void EvasionBehaviour()
    {
      /*  evade = Vector3.zero;

        float distance = Vector3.Distance(target.position, transform.position); // Get Distanc to target
        float t = distance / speed;

        evade = target.position - transform.position;

        // Create Steering Vector
        var evasionForce = evade.normalized * speed;

        // Add Steering Vector to desired;
        //desired += evasionForce * -pursueWeight; */

        desired += transform.right.normalized * pursueWeight;
    }

    public void PursuitBehaviour()
    {
        pursue = Vector3.zero;

        // Behaviour Pursue
        // Use Velocity to predict future position relativ to own position
        Vector3 inFront = targetMov.velocity;
        inFront = inFront.normalized * 2;
        pursue = target.position + inFront;

        pursue = pursue - transform.position;
        /// TEMP
        Debug.DrawRay(transform.position, pursue, Color.cyan);

        float distance = Vector3.Distance(target.position, transform.position); // Get Distanc to target

        // Behaviour Evasion
        if (distance < arrivalDistance) // if object is in Radius
        {
            EvasionBehaviour();
        }
        else
        {
            pursue = pursue.normalized * speed;
        }

        // Create Steering Vector
        var pursueForce = pursue.normalized * speed;

        // Add Steering Vector to desired;
        desired += pursueForce * pursueWeight;
    }

    public void FollowingBehaviour()
    {
        pursue = Vector3.zero;

        // Behaviour Pursue
        // Use Velocity to predict future position relativ to own position
        Vector3 inFront = targetMov.velocity;
        inFront = inFront.normalized * -3;
        pursue = target.position + inFront;

        pursue = pursue - transform.position;

        /// TEMP
        Debug.DrawRay(transform.position, pursue, Color.cyan);

        float distance = Vector3.Distance(target.position, transform.position); // Get Distanc to target

        // Behaviour Evasion
        if (distance < arrivalDistance) // if object is in Radius
        {
            EvasionBehaviour();
        }
        else
        {
            pursue = pursue.normalized * speed;
        }

        // Create Steering Vector
        var pursueForce = pursue.normalized * speed;

        // Add Steering Vector to desired;
        desired += pursueForce * pursueWeight;
    }

    public void SeekBehaviour()
    {
        if (nextPickup != null)
        {
            Vector3 seek = Vector3.zero;

            seek = nextPickup.position - transform.position;

            /// TEMP
            Debug.DrawRay(transform.position, seek, Color.cyan);

            if (target != null)
            {
                float distance = Vector3.Distance(target.position, transform.position); // Get Distanc to target

                // Behaviour Evasion
                if (distance < arrivalDistance) // if object is in Radius
                {
                    EvasionBehaviour();
                }
                else
                {
                    seek = seek.normalized * speed;
                }
            }

            // Add Steering Vector to desired;
            desired += seek * pursueWeight;
        } 
    }

    Vector3 SteerTowards(Vector3 vector)
    {
        Vector3 v = vector.normalized * maxSpeed - velocity;
        return Vector3.ClampMagnitude(v, maxSteerForce);
    }

    public void SetTarget()
    {
        if (GameObjectManager.players.Count > 0 && Scoring.playing)
        {
            target = GameObjectManager.GetClosestPlayer(transform.position);
            targetMov = target.GetComponent<PlayerBodyMovement>();

            // Look if PickUps exist and the NPC is actually searching for it
            if(GameObjectManager.pickUps.Count > 0 && getPickUp)
            {
                nextPickup = GameObjectManager.GetClosestPickUp(transform.position);

                if(Vector3.Distance(transform.position, nextPickup.position) <= pickUpDistance)
                {
                    state = State.pickUp;
                }
            }
        }
    }

    public void DeactivateEnemy()
    {
        if (Scoring.playing)
        {
            health.health = 0;
            foreach (EnemyBehaviour eb in GameObjectManager.targetBehaviours)
            {
                eb.neighbours.Remove(this);
            }

            GameObjectManager.targets.Remove(transform);
            GameObjectManager.targetBehaviours.Remove(this);
            if (GameObjectManager.targets.Count == 0)
            {
                Scoring.roundManager.SetWinnerText("Player");
            }
        }
    }

private void OnDestroy()
    {
        if (Scoring.playing) {
            health.health = 0;
        foreach(EnemyBehaviour eb in GameObjectManager.targetBehaviours)
        {
            eb.neighbours.Remove(this);
        }
            
            GameObjectManager.targets.Remove(transform);
            GameObjectManager.targetBehaviours.Remove(this);
            if (GameObjectManager.targets.Count == 0)
            {
                Scoring.roundManager.SetWinnerText("Player");
            }
        }
    }

    bool IsHeadingForCollision()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, boundsRadius, forwardDir, out hit, collisionAvoidDst, LayerMask.GetMask("Obstacle")))
        {
            return true;
        }
        else { }
        return false;
    }

    Vector3 ObstacleRays()
    {
        Vector3[] rayDirections = BoidHelper.directions;

        for (int i = 0; i < rayDirections.Length; i++)
        {
            Vector3 dir = transform.TransformDirection(rayDirections[i]);
            Ray ray = new Ray(transform.position, dir);
            if (!Physics.SphereCast(ray, boundsRadius, collisionAvoidDst, LayerMask.GetMask("Obstacle")))
            {
                Debug.DrawRay(transform.position, dir, Color.green, 5);
                return dir;
            }
        }

        return transform.forward;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, boundsRadius);
        Gizmos.DrawWireSphere(pointToDraw, 1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.CompareTag("Death"))
        {
            Destroy(gameObject);
        }
    }
     // Emitt Steering Sound
    IEnumerator SteeringSound(){
        if(motorEmitter != null){
            Debug.Log("MotorParameter Changed");
            motorEmitter.SetParameter("SteerValue",0.11f);
            yield return new WaitForSeconds(0.5f);
            motorEmitter.SetParameter("SteerValue",0f);
        }
        yield break;
    }
}
