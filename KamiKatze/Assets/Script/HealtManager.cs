﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealtManager : MonoBehaviour
{
    public int health = 5;
    public bool invincible = false;
    public List<Renderer> rend = new List<Renderer>();
    public Color initialCol;

    public Transform smokeRoot;
    List<GameObject> smokeVFX = new List<GameObject>();

    private void Start()
    {
        rend.Add(transform.Find("PlaneGame_Plane/Flügel").GetComponent<Renderer>());
        rend.Add(transform.Find("PlaneGame_Plane/mainbody").GetComponent<Renderer>());

        initialCol = rend[0].material.GetColor("_BaseColor");

        foreach(Transform t in smokeRoot)
        {
            smokeVFX.Add(t.gameObject);
            t.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void Damage()
    {
      if(!invincible)
            StartCoroutine(InvincibleFrames());
    }

    public IEnumerator InvincibleFrames()
    {
        foreach (Renderer r in rend)
        {
            r.material.SetColor("_BaseColor", Color.gray);
        }
        health--;
        invincible = true;
        smokeVFX[health].SetActive(true);
        yield return new WaitForSeconds(1f);
        invincible = false;

        foreach (Renderer r in rend)
        {
            r.material.SetColor("_BaseColor", initialCol);
        }
    }
}
