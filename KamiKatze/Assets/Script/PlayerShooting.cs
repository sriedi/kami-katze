﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public enum Shot {
    normal,
    searching,
    mine, 
    shotgun,
    burst,
    shield
}

public class PlayerShooting : MonoBehaviour
{
    public Transform[] muzzles;
    public Transform[] shotgunMuzzles;
    public List<GameObject> guns = new List<GameObject>();
    public List<GameObject> shotguns = new List<GameObject>();
    public GameObject rocketProb;
    public GameObject mineProb;
    public Transform specialMuzzle;
    public GameObject bullet;
    public GameObject targetSeekingRocket;
    public GameObject mine;
    public Transform shield;
    public PlayerTopDownMovement brain;
    HealtManager health;

    public Shot pickup;
    Player player;
    public bool firing = false;
    public float intervalTime = 0.5f;
    float initialIntervalTime;
    float timer;
    int maxPickupIterations = 30;
    int shotGunIterations = 3;

    //GunEmitter from the GunsoundEmitter Prefab
    GunEmitter gunSoundEmitter;

    [Header("UI")]
    public UiHandler ui;

    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<HealtManager>();
        player = ReInput.players.GetPlayer(brain.playerId);
        timer = intervalTime;
        initialIntervalTime = intervalTime;
        shield.gameObject.SetActive(false);
        gunSoundEmitter = gameObject.GetComponentInChildren<GunEmitter>();

        foreach(Transform m in muzzles)
        {
            if (m.Find("RapidFire")) //name.Contains("RapdiFire"))
            {
                GameObject g = m.Find("RapidFire").gameObject;
                guns.Add(g);
                g.SetActive(false);
            }

            foreach (Transform n in shotgunMuzzles)
            {
                if (n.Find("Shotgun")) //name.Contains("RapdiFire"))
                {
                    GameObject g = n.Find("Shotgun").gameObject;
                    shotguns.Add(g);
                    g.SetActive(false);
                }


                if (n.Find("Mine"))
                {
                    mineProb = n.Find("Mine").gameObject;
                    mineProb.SetActive(false);
                }

                if (n.Find("Rocket"))
                {
                    rocketProb = n.Find("Rocket").gameObject;
                    rocketProb.SetActive(false);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Scoring.playing)
        {
            ui.pickUp = pickup;

            DisplayGuns();

            if (player.GetButtonDown("Shoot"))
            {
                firing = true;
                if (pickup != Shot.burst)
                {
                    timer = 0;
                }
            }

            if (player.GetButtonUp("Shoot"))
            {
                if (pickup != Shot.burst)
                {
                    firing = false;
                    timer = 0;
                }
            }

            if (firing)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    timer = intervalTime;
                    Shooting();
                }
            }
        }
    }

    public void DisplayGuns()
    {
        if (pickup == Shot.burst)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(true);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        }
        else if (pickup == Shot.searching)
        {
            rocketProb.SetActive(true);
            mineProb.SetActive(false);

            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }
        }
        else if (pickup == Shot.mine)
        {
            mineProb.SetActive(true);
            rocketProb.SetActive(false);

            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }
        }
        else if (pickup == Shot.shotgun)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(true);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        }
        else if (pickup == Shot.normal)
        {
            foreach (GameObject g in shotguns)
            {
                g.SetActive(false);
            }

            foreach (GameObject g in guns)
            {
                g.SetActive(false);
            }

            rocketProb.SetActive(false);
            mineProb.SetActive(false);
        }
    }

    public void ActivateShield()
    {
        shield.gameObject.SetActive(true);
        health.invincible = true;
        StartCoroutine(DeactivateShield());
    }

    public void Shooting()
    {
        if (pickup == Shot.normal)
        {
             gunSoundEmitter.ActionSound("gun");
            intervalTime = initialIntervalTime;
            foreach (Transform t in muzzles)
            {
                GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                g.GetComponent<BulletBehaviour>().parent = transform;
            }
        }
        else if (pickup == Shot.searching)
        {
            GameObject g = Instantiate(targetSeekingRocket, specialMuzzle.transform.position, specialMuzzle.transform.rotation, null);
            g.GetComponent<SeekingRocket>().parent = transform;
            g.GetComponent<SeekingRocket>().shield = shield;
            pickup = Shot.normal;
        }
        else if (pickup == Shot.mine)
        {
            GameObject g = Instantiate(mine, specialMuzzle.transform.position, specialMuzzle.transform.rotation, null);
            g.GetComponent<MineBehaviour>().parent = transform;
            g.GetComponent<MineBehaviour>().shield = shield;
            pickup = Shot.normal;
        }
        else if (pickup == Shot.burst)
        {
            if (maxPickupIterations >= 0)
            {
                maxPickupIterations--;
                intervalTime = initialIntervalTime * 0.2f;
                gunSoundEmitter.ActionSound("gun");
                foreach (Transform t in muzzles)
                {
                    GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                    g.GetComponent<BulletBehaviour>().parent = transform;
                }
            }
            else
            {
                maxPickupIterations = 30;
                firing = false;
                pickup = Shot.normal;
            }
        } else if (pickup == Shot.shotgun)
        {
            if (shotGunIterations >= 0)
            {
                shotGunIterations--;
                intervalTime = initialIntervalTime * 1.2f;
                gunSoundEmitter.ActionSound("gun");

                foreach (Transform t in shotgunMuzzles)
                {
                    GameObject g = Instantiate(bullet, t.position, t.rotation, null);
                    BulletBehaviour b = g.GetComponent<BulletBehaviour>();
                    b.parent = transform;
                    b.lifetime *= 0.2f;
                }
            }
            else
            {
                shotGunIterations = 3;
                firing = false;
                pickup = Shot.normal;
            }
        }
    }

    IEnumerator DeactivateShield()
    {
        yield return new WaitForSeconds(3);
        health.invincible = false;
        shield.gameObject.SetActive(false);
    }
}
