﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiHandler : MonoBehaviour
{
    public HealtManager health;
    public Shot pickUp;

    [Header("Data")]
    public Sprite[] pickups;
    public Sprite healthSprite;
    public Sprite noHealthSprite;

    [Header("Rendering")]
    public Image currentPickup;
    public Image[] currentHealth;

    // Start is called before the first frame update
    void Awake()
    {
        if(health.transform.CompareTag("Player"))
        {
            health.transform.GetComponent<PlayerShooting>().ui = this;
        } else
        {
            health.transform.GetComponent<EnemyShooting>().ui = this;
        }
    }

    private void Update()
    {
        if (Scoring.playing)
        {
            currentPickup.sprite = pickups[(int)pickUp];

            if (health.invincible)
            {
                int d = 0;

                foreach (Image i in currentHealth)
                {
                    if (d < health.health)
                    {
                        i.sprite = healthSprite;
                    }
                    else
                    {
                        i.sprite = noHealthSprite;
                    }

                    d++;
                }
            }
        }
    }
}
