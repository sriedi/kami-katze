﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerMovement : MonoBehaviour
{
    [Header("Setup")]
    public int playerId;
    public Camera mainCam;
    [Header("Behaviour")]
    public float maxVelocity = 10f;
    public float throttleSpeed = 5f;
    public float speed;
    float spd;

    Transform cachedTransform;
    Vector3 position;
    public Vector3 velocity;

    [HideInInspector]
    public Player player;

    public Transform ground;
    float horizontal = 0;
    float vertical = 0;
    float evenPlane;

    // Start is called before the first frame update
    void Start()
    {
        GameObjectManager.players.Add(transform);
        player = ReInput.players.GetPlayer(playerId);

        cachedTransform = transform;
        position = cachedTransform.position;
        velocity = cachedTransform.forward;

        speed = maxVelocity;
        spd = speed;
    }

    private void Update()
    {
         horizontal = player.GetAxis("Horizontal");
         vertical = player.GetAxis("Vertical");

        if (player.GetButton("toLeft"))
        {
            evenPlane += 0.05f;
        } else if (player.GetButton("toRight"))
        {
            evenPlane -= 0.05f;
        } else
        {
            evenPlane = 0;
            //transform.rotation.z = Mathf.Lerp(evenPlane, 0, Time.deltaTime);
        }

        if (player.GetButtonDown("Throttle"))
        {
            throttleSpeed = 0.5f;
        } else if (player.GetButtonUp("Throttle"))
        {
            throttleSpeed = 1;
        } 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        spd *= throttleSpeed;

        /// Movy movy

        Vector3 velocity = transform.forward * spd * Time.deltaTime;
       // velocity.Normalize();

        transform.position += velocity;

        speed -= transform.forward.y * Time.deltaTime * 5f;

        if(speed < 7f)
        {
            speed = 7f;
        } else if (speed > maxVelocity)
        {
            speed = maxVelocity;
        }

        spd = speed;

        //transform.Rotate(vertical, horizontal/2, -horizontal);
        transform.Rotate(vertical, horizontal, evenPlane);
    }
}
