﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Lightning : MonoBehaviour
{
    public AnimationCurve curve;
    public float maxValue;

    float minTime = 0.2f;
    float thresh = .8f;


    private float lastTime = 0;
    private Light myLight;
    private StudioEventEmitter thunderEmitter;

    void Start()
    {
        thunderEmitter = GetComponent<StudioEventEmitter>();
        myLight = GetComponent<Light>();
        StartCoroutine(RandomLightning());
    }


    void Update()
    {

    }

    IEnumerator RandomLightning()
    {
        float timer = 0.0f;
        float r = Random.Range(0.05f, minTime);


        float t = Random.Range(0.4f, 2);

        while (timer <= r)
        {
            myLight.intensity = Mathf.Lerp(0, maxValue, curve.Evaluate(timer / r));
            if(thunderEmitter != null){
                thunderEmitter.Play();
            }
            timer += Time.deltaTime;
            yield return null;
        }

        myLight.intensity = 0;

        yield return new WaitForSeconds(t);
        StartCoroutine(RandomLightning());
    }
}