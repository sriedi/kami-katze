﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerTopDownMovement : MonoBehaviour
{
    public int playerId = 0;
    public float speed = 4f;
    public Vector3 acceleration;
    public Vector3 startDirection = new Vector3(1, 0, 0);
    Vector3 lastVelocity = Vector3.zero;

    public Player player;
    float v;
    float h;

    private void Awake()
    {
        player = ReInput.players.GetPlayer(playerId);
        lastVelocity = startDirection;
        GetComponent<Renderer>().enabled = false;
    }

    private void Update()
    {

        v = player.GetAxis("Vertical");
        h = player.GetAxis("Horizontal");

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Scoring.playing)
        {
            acceleration = Vector2.zero;


            Vector3 movement = new Vector2(h, v);
            if (movement.magnitude > 0f)
            {
                lastVelocity = movement;
                acceleration += movement.normalized;
            }
            else
            {
                acceleration += lastVelocity.normalized;
            }

            transform.position += acceleration * Time.deltaTime * speed;
        }
    }
}
