﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereRotationScript : MonoBehaviour
{
    public float xRot;
    public float yRot;
    public float zRot;

    public float rotationSpeedX = 3f;
    public float rotationSpeedY = 3f;
    public float rotationSpeedZ = 3f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        xRot = rotationSpeedX * Time.deltaTime;
        yRot = rotationSpeedY * Time.deltaTime;
        zRot = rotationSpeedZ * Time.deltaTime;

        transform.Rotate(xRot, yRot, zRot);
    }
}
