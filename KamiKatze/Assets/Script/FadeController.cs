﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeController : MonoBehaviour
{
    public Animator rend;

    public void FadeOut()
    {
        rend.SetBool("Fade", true);
    }
}
