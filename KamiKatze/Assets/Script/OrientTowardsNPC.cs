﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrientTowardsNPC : MonoBehaviour
{
    public int enemyId = 0;
    public Transform target;
    Transform parent;

    SpriteRenderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();

        if (enemyId < GameObjectManager.targets.Count)
        {
            target = GameObjectManager.targets[enemyId];
            parent = transform.parent;
            rend.color = target.GetComponent<EnemyShooting>().ui.GetComponent<Image>().color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Scoring.playing)
        {
            if (target != null)
            {
                transform.right = target.position - parent.position;
            } else
            {
                rend.enabled = false;
            }
        }
    }
}
