﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorHelper : MonoBehaviour
{
    public EnemyBehaviour brain;

    private void Awake()
    {
        brain = transform.parent.GetComponent<EnemyBehaviour>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("NPC") && other.transform != brain.transform)
        {
            brain.neighbours.Add(other.GetComponent<EnemyBehaviour>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("NPC"))
        {
            brain.neighbours.Remove(other.GetComponent<EnemyBehaviour>());
        }
    }
}
