﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject spawn;
    public List<Transform> spawnPoints = new List<Transform>();
    public float minTime = 4;
    public float maxTime = 7;

    bool trigger = true;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform t in transform)
        {
            spawnPoints.Add(t);
        }
    }

    private void Update()
    {
        if(Scoring.playing && trigger)
        {
            StartCoroutine(SpawnItem(0.5f));
            trigger = false;
        }
    }

    IEnumerator SpawnItem(float time)
    {
        if (Scoring.playing)
        {
           // print("spawning");
            yield return new WaitForSeconds(time);
            int r = Random.Range(0, spawnPoints.Count);
            float t = Random.Range(minTime, maxTime);

            Instantiate(spawn, spawnPoints[r].position, Quaternion.identity);
            StartCoroutine(SpawnItem(t));
        }
    }
}
