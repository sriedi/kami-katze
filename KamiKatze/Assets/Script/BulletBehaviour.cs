﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public float speed = 12f;
    public float lifetime = 2;
    Rigidbody rb;
    Vector3 velocity;
    Vector3 desired;

    public Transform parent;
    public GameObject impact;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(DestroySelf());
        velocity = transform.forward * speed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        Vector3 steering = Vector3.zero;
        // rb.velocity += transform.forward * speed * Time.deltaTime;

        // Create Steering Vector
        var forwardForce = transform.forward.normalized * speed;

        // Add Steering Vector to desired;
        desired += forwardForce * 20;

        // Create steering Force
        steering = desired - velocity;
        steering = Vector3.ClampMagnitude(steering, 6);

        // Locomotion
        velocity = Vector3.ClampMagnitude(velocity + steering, speed);
        velocity.z = 0;
        transform.position += velocity* Time.deltaTime;
}

    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (Scoring.playing && collision.transform != null && parent != null)
        {
            if (collision.transform != parent && collision.transform != parent.Find("Shield") && !collision.name.Contains("Sensor") && collision.transform != null)
            {
                if (collision.transform.CompareTag("NPC") || collision.transform.CompareTag("Player"))
                {
                    HealtManager h = collision.transform.GetComponent<HealtManager>();

                    if (!h.invincible)
                    {
                        h.Damage();
                    }
                }
                Instantiate(impact, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}
