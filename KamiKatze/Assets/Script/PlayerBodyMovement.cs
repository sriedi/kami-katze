﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class PlayerBodyMovement : MonoBehaviour
{
    Vector3 desired;
    [HideInInspector]
    public Vector3 velocity;
    Vector3 seek;

    public PlayerTopDownMovement brain;
    public Transform target;
    public float maxSpeed = 5f;
    public float maxSteerForce = 3f;

    Vector3 lastVelocity = Vector3.zero;

    float evenPlane = 0;

    //SoundEmitter
    StudioEventEmitter motorEmitter;
    private void Awake()
    {
        GameObjectManager.players.Add(transform);
    }

    // Start is called before the first frame update
    void Start()
    {
        brain = transform.parent.GetComponentInChildren<PlayerTopDownMovement>();
        velocity = transform.forward * maxSpeed * Time.deltaTime;
        motorEmitter = gameObject.GetComponent<StudioEventEmitter>();
    }

    private void Update()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Scoring.playing)
        {
            // Reset desired
            Vector3 steering = Vector3.zero;
            // maxSpeed = brain.speed + 0.01f;

            // reset desired Velocity, very Important for arrival Behaviour
            desired = Vector3.zero;
            //  velocity = transform.forward * maxSpeed * Time.deltaTime;

            // Get direction
            seek = target.position - transform.position;

            // Create Steering Vector
            var seekForce = seek.normalized * maxSpeed;

            // Add Steering Vector to desired;
            desired += seekForce * 1;

            // Create steering Force
            steering = desired - velocity;
            steering = Vector3.ClampMagnitude(steering, maxSteerForce);
            steering.z = 0;

            // Locomotion
            velocity = Vector3.ClampMagnitude(velocity + steering, maxSpeed);
            velocity.z = 0;
            transform.position += velocity * Time.deltaTime;

            // Rotation
            // transform.rotation = Quaternion.LookRotation(velocity);
            transform.right = velocity;

            //float angle = AngleInDeg(velocity, desired);
            float angle = Vector3.Angle(lastVelocity, velocity);
            //Check Angle to Emit Sound
            if(angle >= 6f){
                StartCoroutine(SteeringSound());
            }
           // print(angle);
            float sign = Mathf.Sign(Vector3.Dot(lastVelocity, velocity));

            Vector3 pos = transform.position;
            pos.z = 0;
            transform.position = pos;

            transform.Rotate(new Vector3(angle * 10, 0, 0));
            lastVelocity = velocity;
        }
    }

    float AngleDir(Vector3 fwd, Vector3 targetDir)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, Vector3.up);

        return Mathf.Sign(dir);
    }

    //This returns the angle in radians
    public static float AngleInRad(Vector3 vec1, Vector3 vec2)
    {
        return Mathf.Atan2(vec2.y - vec1.y, vec2.x - vec1.x);
    }

    //This returns the angle in degrees
    public static float AngleInDeg(Vector3 vec1, Vector3 vec2)
    {
        return AngleInRad(vec1, vec2) * 180 / Mathf.PI;
    }

    public void DeactivatePlayer()
    {
        if (Scoring.playing)
        {
            //GameObjectManager.players.Remove(transform);
            GameObjectManager.players[0].gameObject.SetActive(false);
            target.GetComponent<PlayerTopDownMovement>().speed = 0;
            Scoring.roundManager.SetWinnerText("Enemies");
        }
    }

    private void OnDestroy()
    {
        if (Scoring.playing)
        {
            if (Scoring.multiplayer)
            {
                string s = GameObjectManager.ReturnWinnerName(transform);
                Scoring.roundManager.SetWinnerText(s);
            }
            else
            {
                Scoring.roundManager.SetWinnerText("Enemies");
            }

            GameObjectManager.players.Remove(transform);
            target.GetComponent<PlayerTopDownMovement>().speed = 0;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Death"))
        {
            Destroy(gameObject);
        }
    }

    // Emitt Steering Sound
    IEnumerator SteeringSound(){
        if(motorEmitter != null){
            Debug.Log("MotorParameter Changed");
            motorEmitter.SetParameter("SteerValue",0.11f);
            yield return new WaitForSeconds(0.5f);
            motorEmitter.SetParameter("SteerValue",0f);
        }
        yield break;
    }
}
