﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    float speed = -15;
    List<Transform> obstacle = new List<Transform>();
    public GameObject impact;

    private void Awake()
    {
        foreach(Transform t in transform)
        {
            obstacle.Add(t);
            t.gameObject.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        int r = Random.Range(0, obstacle.Count);
        obstacle[r].gameObject.SetActive(true);

        speed = Random.Range(speed - 2, speed + 2);
        StartCoroutine(DestroySelf());
    }

    // Update is called once per frame
    void Update()
    {
        if (Scoring.playing)
        {
            transform.position += Vector3.forward * Time.deltaTime * speed;
        }
    }

    IEnumerator DestroySelf()
    {
        if (Scoring.playing)
        {
            yield return new WaitForSeconds(10);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (Scoring.playing && collision.transform != null)
        {
                if (collision.transform.CompareTag("NPC") || collision.transform.CompareTag("Player"))
                {
                    HealtManager h = collision.transform.GetComponent<HealtManager>();

                    if (!h.invincible)
                    {
                        h.Damage();
                    }
                }
                Instantiate(impact, collision.transform.position, Quaternion.identity);
             //   Destroy(gameObject);
            }
        }
}
