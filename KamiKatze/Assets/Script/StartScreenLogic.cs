﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;

public class StartScreenLogic : MonoBehaviour
{
    public FadeController fade;
    Player player;
    public GameObject panel;
    bool open = false;

    // Start is called before the first frame update
    void Start()
    {
        player = ReInput.players.GetPlayer(0);
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetButtonDown("Y"))
        {
            fade.FadeOut();
            StartCoroutine(LoadFirstLevel(1));
        } else if (player.GetButtonDown("X"))
        {
            Application.Quit();
        }
        else if (player.GetButtonDown("B"))
        {
            fade.FadeOut();
            StartCoroutine(LoadFirstLevel(4));
        }
        else if (player.GetButtonDown("Shoot"))
        {
            //fade.FadeOut();
            if(open)
            {
                panel.SetActive(false);
                StartCoroutine(SetBool(false));
            } else
            {
                panel.SetActive(true);
                StartCoroutine(SetBool(true));
            }
        }
    }

IEnumerator LoadFirstLevel(int i)
    {
        yield return new WaitForSeconds(1.1f);
        GameObjectManager.ClearManager();
        SceneManager.LoadScene(i);
    }

    IEnumerator SetBool(bool b)
    {
        yield return new WaitForSeconds(0.5f);
        open = b;
    }
}
